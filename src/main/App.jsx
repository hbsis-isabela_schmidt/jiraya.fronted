import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Private from '../telas/private/template/index';
import Login from '../telas/public/login/Login';

export default props =>

    <Router>

        <Switch>

            <Route exact path='/' render={() => (

                <Redirect to='/home' />

            )} />
            <Route exact path='/login' component={Login} />
            <Route parth='/home' component={Private} />

        </Switch>

    </Router>
