import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../../../node_modules/font-awesome/css/font-awesome.min.css'

import './index.css'

import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router-dom'

import Footer from '../components/template/Footer'
import Routes from './Routes';
import Header from '../components/template/Header';

export default class Template extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false
        }
    }

    componentWillMount() {
        if (sessionStorage.getItem('token')) {
            console.log('Autenticado!');
        } else {
            this.setState({ redirect: true })
        }
    }

    render() {

        if (this.state.redirect) {
            return (<Redirect to={'/login'} />)
        }

        return (

            <Fragment>

                <Header />

                <div className='container-fluid'>

                    <Routes />                    

                </div>

                <Footer />

            </Fragment>

        )

    }

}
