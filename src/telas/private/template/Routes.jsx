import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from '../components/home/Home'
import History from '../components/history/History'
import HowWorks from '../components/howWorks/HowWorks'
import Jira from '../components/Jira/Jira'
import Creditos from '../components/creditos/Creditos'

export default props =>
    <Switch>

        <Route exact path='/home' component={Home} />
        <Route path='/history' component={History} />
        <Route path='/Jira'component={Jira}/>
        <Route path='/how-works' component={HowWorks} />
        <Route path='/Creditos' component={Creditos} />

        
        <Redirect from='*' to='/home' />

    </Switch>