import React from 'react'
import './HowWorks.css'

export default props =>
    <div className="box">
        <p>Com Jiraya você pode fazer suas marcações de ponto de forma mais rápida, fácil e intuitiva.</p>
        <p>O sistema armazena suas marcações de ponto na maquina que está sendo realizada. Evite apagar historico, cookies e cache do navegador para não atrapalhar as funcionalidades do site.</p>
        <p>Agradecemos por usar o Jiraya! xD</p>
    </div>