import React, { Component } from 'react'
import { Jumbotron, Button } from 'react-bootstrap'
import './Jira.css'

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: 'Nenhuma atividade encontrada'
        }
    }
    render() {
        return (
            <div className='row'>
                <div className='shadow-jumb'>
                    <Jumbotron>
                        <div className='row'>
                            <div className='col-sm-3 offset-sm-1'>
                                <div className='form-group'>
                                    <label className='control-label' htmlFor='campo-data'>Tempo Gasto</label>
                                    <input onChange={this.handleChange} value={this.state.dataInicio} name='dataInicio' id='campo-data' type='text' className='form-control form-control-width'></input>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='from-group '>
                                    <label className='control-label' htmlFor='campo-data2'>Nome do Jira</label>
                                    <input onChange={this.handleChange} value={this.state.dataFim} name='dataFim' id='campo-data2' type='text' className='form-control form-control-width'></input>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='from-group '>
                                    <label className='control-label' htmlFor='campo-data2'>Descrição do trabalho</label>
                                    <input onChange={this.handleChange} value={this.state.dataFim} name='dataFim' id='campo-data2' type='text' className='form-control form-control-width'></input>
                                </div>
                            </div>
                            <div className='col-sm-2'>
                                <input onClick={this.sendDate} type="submit" id='btn-enviar-jira' />
                            </div>
                        </div>
                    </Jumbotron>
                </div>
            </div>
        )
    }
}