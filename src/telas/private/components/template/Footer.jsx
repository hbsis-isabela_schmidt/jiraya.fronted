import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Button } from 'react-bootstrap'

import './Footer.css'

export default class Footer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    handleLogout() {
        sessionStorage.setItem('username', '')
        sessionStorage.setItem('password', '')
        sessionStorage.setItem('token', '')
        sessionStorage.clear()
        this.setState({ redirect: true })
    }

    render() {

        if (this.state.redirect) {
            return (<Redirect to={'/login'} />)
        }

        return (

            <footer className="footer bg-hb">
                <div className="container">
                    <span className="text-light">Developed by WMS Team.</span>
                </div>

                <div id='button-align-logout'>
                    <div className="btn-group dropup">
                        <button type="button" className="btn btn-color" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><i className='fas fa-arrow-up'></i></button>
                        <div className="dropdown-menu">
                            <li>
                                <Button className='dropdown-item borda'><i className="fas fa-cogs"></i> Configurações</Button>
                            </li>
                            <li>
                                <Button onClick={this.handleLogout} className='dropdown-item'><i className="fas fa-sign-out-alt "></i> Sair</Button>
                            </li>
                        </div>
                    </div>
                    {/* <div className="dropup">
                        <button className="dropbtn"><i className='fas fa-chevron-up'></i></button>
                        <div className="dropup-content">
                            <a>Configurar</a>
                            <a>Sair</a>
                        </div>
                    </div> */}
                    {/* <Button onClick={this.handleLogout} className='style-button'><i className="fas fa-sign-out-alt "></i></Button> */}
                </div>
            </footer>
        )
    }

}