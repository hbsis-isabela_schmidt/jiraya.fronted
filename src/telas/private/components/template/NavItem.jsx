import React, { Component } from 'react'

import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';


class NavItem extends Component {

    render() {

        var isActive = this.context.router.route.location.pathname === this.props.to;
        var className = isActive ? 'active' : '';

        return (

            <Link style={{ textDecoration: 'none' }} className={className} {...this.props}>
                {this.props.children}
            </Link>

        )
    }
}

NavItem.contextTypes = {
    router: PropTypes.object
};

export default NavItem;