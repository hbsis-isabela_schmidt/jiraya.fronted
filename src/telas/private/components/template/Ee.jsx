import React, { Component } from 'react'
import EasterEgg from 'react-easter'


export default class Eegg extends Component {
    render() {
        const konamiCode = [
            'arrowup',
            'arrowup',
            'arrowdown',
            'arrowdown',
            'arrowleft',
            'arrowright',
            'arrowleft',
            'arrowright',
            'b',
            'a',
            'enter'
        ];

        return (
            <EasterEgg keys={konamiCode}
                timeout={10000}>
                <div className="overlay">
                    <iframe title="KonamiCode"
                        src="https://www.youtube.com/embed/DLzxrzFCyOs?autoplay=1"
                        frameBorder="0"
                        allowFullscreen />
                </div>
            </EasterEgg>
        );
    }
}