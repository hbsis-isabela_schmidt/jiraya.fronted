import React, { Component } from 'react'

import './Header.css'
import './NavItem.css'

import NavItem from './NavItem'
import Logo from '../../../../assets/images/logo.svg'


export default class Header extends Component {

    state = {
        active: false,
    }

    toggleClass() {
        console.log(this.state.active)
        this.setState = ({
            active: true,
        })
    }

    render() {
        return (

            <nav className="navbar navbar-expand-lg navbar-dark bg-hb">
                <a className="navbar-brand" href="/home"><img src={Logo} className="img" alt='logo-hb' /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">

                        <NavItem to='/home'>
                            <p href={`/home`} className={`nav-item nav-link p-margin-0`}>Ponto</p>
                        </NavItem>
                        <NavItem to='/history'>
                            <p href={`/history`} className={`nav-item nav-link p-margin-0`}>Histórico de Pontos</p>
                        </NavItem>
                        <NavItem to='/jira'>
                            <p href={`/jira`} className={`nav-item nav-link p-margin-0`}>Jira</p>
                        </NavItem>
                        <NavItem to='/how-works'>
                            <p href={`/how-works`} className={`nav-item nav-link p-margin-0`}>Como Funciona?</p>
                        </NavItem>
                        <NavItem to='/Creditos' className='isDisabled'>
                            <p href={`/Creditos`} className={`nav-item nav-link p-margin-0`}>Creditos</p>
                        </NavItem>
                    </div>
                </div>
            </nav>


        )
    }


}