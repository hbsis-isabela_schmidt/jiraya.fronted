import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Moment from 'react-moment'
import 'moment/locale/pt-br'
import './timer/LastHitPoint.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Timer from './timer/Timer'
import { PontoMethod } from '../../../../../services/PostData'
import { SaveHistory, SaveLastPoints } from '../../../../../services/LocalStorage'
import { LoginsDia } from '../../../../../services/Logins'
import { HistoryDia, PontosTrabalhoDia, PontosIntervaloDia } from '../../../../../services/PontosDia'
import TimerAtual from './timer/TimerAtual'
import Eegg from '../../template/Ee'
import './Ponto.css'
import ModalAdicionar from '../modal/ModalAdd'

let dayLastRegister
let monthLastRegister
let yearLastRegister
let hourLastRegister
let minutesLastRegister
let secondsLastRegister
let data

class Ponto extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userLocal: '',
            userSession: '',
            running: true,
            hours: 0,
            minutes: 0,
            seconds: 0,
            horasTrabalhadas: 0,
            isLoading: false,
            primeiroLoginJiraya: this.props.primeiroLoginJiraya,
            mensagemTipoHorario: '',
            mensagemClass: '',
            showModalAdd: false
        }
        this.handleClick = this.handleClick.bind(this)
        this.notifyRegisterError = this.notifyRegisterError.bind(this)
        this.notifyRegisterSuccess = this.notifyRegisterSuccess.bind(this)
        this.gerarId = this.gerarId.bind(this)
        this.verificaCount = this.verificaCount.bind(this)
        this.setTimer = this.setTimer.bind(this)
        this.filterDiaAtual = this.filterDiaAtual.bind(this)
        this.msToTime = this.msToTime.bind(this)
        this.setStateTimer = this.setStateTimer.bind(this)
        this.montarAviso = this.montarAviso.bind(this)
        this.jornada = this.jornada.bind(this)
        this.almoco = this.almoco.bind(this)
        this.showModalAdd = this.showModalAdd.bind(this)
        this.hideModalAdd = this.hideModalAdd.bind(this)
    }

    showModalAdd = () => {
        this.setState({ showModalAdd: true })
    }

    hideModalAdd = () => {
        this.setState({ showModalAdd: false })
    }

    notifyRegisterSuccess = () => {
        toast.success("Ponto marcado com sucesso!", {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000
        });
    };

    notifyRegisterError = () => {
        toast.error("Erro ao realizar marcação de ponto!", {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000
        });
    };

    notifyIdError = () => {
        toast.error("Erro ao ao gerar id de um novo card!", {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000
        });
    };

    handleClick() {

        this.setState({ isLoading: true })

        if (localStorage.getItem('history') === null) {
            this.handleSubmit()
        } else {
            const registers = JSON.parse(localStorage.getItem('history'))
            const hourLastRegister = registers[registers.length - 1].hour;

            const firstLogin = LoginsDia();
            const contador = JSON.parse(localStorage.getItem('count'))
            // se o ponto for marcado entre 11am e 2pm
            if (hourLastRegister >= 11 && hourLastRegister < 14) {
                this.almoco()
            }
            // se o ponto for do primeiro login do dia
            else if (firstLogin.length === 1 && contador === 0) {
                this.jornada()
            } else if (firstLogin.length > 1 && contador === 0) {
                this.jornada()
            } else {
                this.handleSubmit()
            }

            // if (!(hourLastRegister >= 11 && hourLastRegister < 14) && !(firstLogin.length === 1 && contador === 0)) {
            //     this.handleSubmit()
            // }
        }
    }

    jornada() {
        const contador = JSON.parse(localStorage.getItem('count'))
        const registers = JSON.parse(localStorage.getItem('history'))
        const dayLastRegister = parseFloat(registers[registers.length - 1].day);

        const d = new Date()
        // se nao completou 11 horas de descanso
        if ((this.state.hours < 11) && (contador === 0) && (d.getDate() !== dayLastRegister)) {
            Swal({
                title: 'Tem certeza?',
                html:
                    'Você não completou 11 horas de descanso! ' +
                    '<a href="/how-works">Saiba Mais!</a> ',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#6f9e32',
                cancelButtonColor: '#fc5457',
                confirmButtonText: 'Sim, marcar ponto!'
            })
                .then((result) => {
                    if (result.value) {
                        this.handleSubmit()
                    }
                })
        }

        if (this.state.hours > 11) {
            this.handleSubmit()
        }

        // if ((this.state.hours > 11) && (contador === 0) && (d.getDate() !== dayLastRegister)) {
        //     this.handleSubmit()
        // }

        this.setState({ isLoading: false })
    }

    almoco() {
        const contador = localStorage.getItem('count');
        const lastRegister = this.PegarDataUltimoRegistro();
        const dataAtual = new Date()

        if (contador === 0) {
            this.handleSubmit()
        } else if (contador % 2 !== 0) {
            this.handleSubmit()
        } else if (contador % 2 === 0) {
            if ((((dataAtual.getTime() - lastRegister.getTime()) / 60000) < 60)) {
                Swal({
                    title: 'Tem certeza?',
                    html:
                        'Você não completou uma hora de almoço! ' +
                        '<a href="/how-works">Saiba Mais!</a> ',
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonColor: '#6f9e32',
                    cancelButtonColor: '#fc5457',
                    confirmButtonText: 'Sim, marcar ponto!'
                })
                    .then((result) => {
                        if (result.value) {
                            this.handleSubmit()
                        }
                    })
            }

            if (((((dataAtual.getTime() - lastRegister.getTime()) / 60000) > 60))) {
                this.handleSubmit()
            }

        }

        this.setState({ isLoading: false })
    }

    PegarDataUltimoRegistro() {
        const registers = JSON.parse(localStorage.getItem('history'));
        const dayLastRegister = registers[registers.length - 1].day;
        const monthLastRegister = registers[registers.length - 1].month;
        const yearLastRegister = registers[registers.length - 1].year;
        const hourLastRegister = registers[registers.length - 1].hour;
        const minutesLastRegister = registers[registers.length - 1].minutes;
        let lastRegister = `${yearLastRegister}-${monthLastRegister}-${dayLastRegister} ${hourLastRegister}:${minutesLastRegister}`;
        lastRegister = new Date(lastRegister);
        return lastRegister;
    }

    gerarId() {
        try {
            if (localStorage.getItem('history')) {
                let getLocal = JSON.parse(localStorage.getItem('history'))
                let count = getLocal.length - 1
                let lastKey = getLocal[count].id
                let newKey = parseFloat(lastKey) + 1
                return newKey
            } else {
                const newKey = 0
                return newKey
            }
        } catch (error) {
            this.notifyIdError()
        }
    }

    handleSubmit() {
        if (sessionStorage.getItem('token')) {
            const userData = { username: sessionStorage.getItem('username'), password: sessionStorage.getItem('password'), token: sessionStorage.getItem('token') }
            PontoMethod('register', userData)
                .then((result) => {
                    const responseJson = result
                    if (responseJson) {
                        this.notifyRegisterSuccess()
                        const date = new Date()
                        SaveHistory({
                            'id': `${this.gerarId()}`,
                            'username': `${userData.username}`,
                            'day': `${date.getDate()}`,
                            'month': `${date.getMonth() + 1}`,
                            'year': `${date.getFullYear()}`,
                            'hour': `${date.getHours()}`,
                            'minutes': `${date.getMinutes()}`,
                            'seconds': `${date.getSeconds()}`
                        }, sessionStorage.getItem('username'))
                        this.verificaCount()
                        this.setTimer()
                        this.update()
                        this.updateScreen()
                        this.setState({ isLoading: false })
                    } else {
                        this.notifyRegisterError()
                        this.setState({ isLoading: false })
                    }
                })
                .catch((res) => {
                    this.notifyRegisterError()
                    this.setState({ isLoading: false })
                })
        }
    }

    setTimer() {
        const count = localStorage.getItem('count');
        const local = JSON.parse(localStorage.getItem('history'));
        const localString = local[local.length - 1]
        if (count % 2 !== 0) {
            SaveLastPoints(localString, 'horaTrabalho')
        }
        if (count % 2 === 0) {
            SaveLastPoints(localString, 'horaIntervalo')
        }
    }

    verificaCount() {
        const localCount = JSON.parse(localStorage.getItem('count'));
        const newCount = localCount + 1;
        localStorage.setItem('count', newCount)
    }

    filterDiaAtual(obj) {
        const d = new Date();
        if (obj.day === d.getDate().toString() && obj.month === ((d.getMonth()) + 1).toString() && obj.year === d.getFullYear().toString() && obj.id % 2 !== 0) {
            return true
        } else {
            return false
        }
    }

    setStateTimer(hours, minutes, seconds) {
        this.setState({ hours: parseFloat(hours) })
        this.setState({ minutes: parseFloat(minutes) })
        this.setState({ seconds: parseFloat(seconds) })
    }

    msToTime(milliseconds) {
        const hours = milliseconds / (1000 * 60 * 60);
        const absoluteHours = Math.floor(hours);

        const minutes = (hours - absoluteHours) * 60;
        const absoluteMinutes = Math.floor(minutes);

        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);

        return { 'hours': `${absoluteHours}`, 'minutes': `${absoluteMinutes}`, 'seconds': `${absoluteSeconds}` };
    }

    takeHours(name, val) {
        const historicoDePontos = JSON.parse(localStorage.getItem(`${name}`));
        const ultimoElementoDoArrayDePontos = historicoDePontos.length - val;

        const dataUltimoPontoMarcado = new Date(
            historicoDePontos[ultimoElementoDoArrayDePontos].year,
            (historicoDePontos[ultimoElementoDoArrayDePontos].month) - 1,
            historicoDePontos[ultimoElementoDoArrayDePontos].day,
            historicoDePontos[ultimoElementoDoArrayDePontos].hour,
            historicoDePontos[ultimoElementoDoArrayDePontos].minutes,
            historicoDePontos[ultimoElementoDoArrayDePontos].seconds);

        const dataAtual = new Date();

        const difereca = this.msToTime(dataAtual - dataUltimoPontoMarcado)

        return difereca
    }

    takeHoursWork() {
        const pontosDoDia = HistoryDia();

        const primeiroPontoDeTrabalhoDia = new Date(
            pontosDoDia[0].year,
            (pontosDoDia[0].month) - 1,
            pontosDoDia[0].day,
            pontosDoDia[0].hour,
            pontosDoDia[0].minutes,
            pontosDoDia[0].seconds);

        const dataAtual = new Date();

        const difereca = this.msToTime(dataAtual - primeiroPontoDeTrabalhoDia)

        return difereca
    }

    tranformarPontosEmDatas() {
        const arrayDePontosTrabalho = PontosTrabalhoDia();
        const arrayDePontosIntervalo = PontosIntervaloDia();
        const newArrayTrabalho = []
        const newArrayIntervalo = []

        for (let i = 0; i < arrayDePontosTrabalho.length; i++) {
            newArrayTrabalho.push(new Date(arrayDePontosTrabalho[i].year, arrayDePontosTrabalho[i].month, arrayDePontosTrabalho[i].day, arrayDePontosTrabalho[i].hour, arrayDePontosTrabalho[i].minutes, arrayDePontosTrabalho[i].seconds));
        }

        for (let i = 0; i < arrayDePontosIntervalo.length; i++) {
            newArrayIntervalo.push(new Date(arrayDePontosIntervalo[i].year, arrayDePontosIntervalo[i].month, arrayDePontosIntervalo[i].day, arrayDePontosIntervalo[i].hour, arrayDePontosIntervalo[i].minutes, arrayDePontosIntervalo[i].seconds));
        }

        return [newArrayTrabalho, newArrayIntervalo]
    }

    toDate() {
        const localStorageHistory = JSON.parse(localStorage.getItem('history'));
        const count = localStorageHistory.length - 1;
        const teste = new Date(localStorageHistory[count].year, localStorageHistory[count].month - 1, localStorageHistory[count].day, localStorageHistory[count].hour, localStorageHistory[count].minutes, localStorageHistory[count].seconds).getTime()
        const dateAtual = new Date().getTime();
        const ms = dateAtual - teste
        return ms
    }

    updateScreen() {
        try {
            const pontosDoDia = HistoryDia();
            if (pontosDoDia.length >= 2) {
                const reducer = (accumulator, currentValue) => accumulator + currentValue;
                const arraysIntervaloTrabalho = this.tranformarPontosEmDatas();
                const arrayTrabalho = arraysIntervaloTrabalho[0];
                const arrayIntervalo = arraysIntervaloTrabalho[1];
                const newArray = [];

                for (let i = 0; i < pontosDoDia.length / 2; i++) {
                    newArray.push(arrayIntervalo[i].getTime() - arrayTrabalho[i].getTime())
                }
                const milliseconds = newArray.reduce(reducer)
                const tempo = this.msToTime(milliseconds);
                localStorage.setItem('totalHorasTrabalhadas', JSON.stringify(tempo))
            }
        } catch (error) {

        }

        try {
            const arrayLoginsDia = LoginsDia()
            const count = JSON.parse(localStorage.getItem('count'))
            // primeiro login do dia
            if (arrayLoginsDia.length === 1) {
                // primeiro ponto em no primeiro logindo do dia
                if (count === 0) {
                    const tempoUltimoPontoMarcado = this.takeHours('history', 1);
                    this.setStateTimer(tempoUltimoPontoMarcado.hours, tempoUltimoPontoMarcado.minutes, tempoUltimoPontoMarcado.seconds)
                    this.setState({ running: true })
                } else {
                    // pontos para de trabalho
                    if (count === 1) {
                        const tempoUltimoPontoMarcado = this.takeHours('history', 1);
                        this.setStateTimer(tempoUltimoPontoMarcado.hours, tempoUltimoPontoMarcado.minutes, tempoUltimoPontoMarcado.seconds)
                        this.setState({ running: true })
                    }
                    if (count % 2 !== 0 && count !== 0) {
                        // const tempoTrabalhado = this.takeHoursWork();
                        // this.setStateTimer(tempoTrabalhado.hours, tempoTrabalhado.minutes, tempoTrabalhado.seconds)
                        // this.setState({ running: true })
                        const toDate = this.toDate()
                        const totalHoras = JSON.parse(localStorage.getItem('totalHorasTrabalhadas'));
                        const horas = totalHoras.hours * 3600000;
                        const minutes = totalHoras.minutes * 60000
                        const seconds = totalHoras.seconds * 1000;

                        const absoluteHours = Math.floor(horas);

                        const absoluteMinutes = Math.floor(minutes);

                        var absoluteSeconds = Math.floor(seconds);

                        const calculo = absoluteHours + absoluteMinutes + absoluteSeconds;

                        const tempo = toDate + calculo;
                        const tempo2 = this.msToTime(tempo)
                        this.setStateTimer(tempo2.hours, tempo2.minutes, tempo2.seconds)
                    }
                    // pontos de intervalo
                    if (count % 2 === 0) {
                        const tempoUltimoPontoIntervalo = this.takeHours('horaIntervalo', 1);
                        this.setStateTimer(tempoUltimoPontoIntervalo.hours, tempoUltimoPontoIntervalo.minutes, tempoUltimoPontoIntervalo.seconds)
                        this.setState({ running: true })
                    }
                }
            }
            // qualquer outro login no dia
            if (arrayLoginsDia.length > 1) {
                if (count === 1) {
                    const tempoUltimoPontoMarcado = this.takeHours('history', 1);
                    this.setStateTimer(tempoUltimoPontoMarcado.hours, tempoUltimoPontoMarcado.minutes, tempoUltimoPontoMarcado.seconds)
                    this.setState({ running: true })
                }
                // primeiro ponto em qualquer login do dia
                if (count === 0) {
                    const tempoUltimoPontoMarcado = this.takeHours('history', 1);
                    this.setStateTimer(tempoUltimoPontoMarcado.hours, tempoUltimoPontoMarcado.minutes, tempoUltimoPontoMarcado.seconds)
                    this.setState({ running: true })
                } else {
                    // pontos para de trabalho
                    if (count % 2 !== 0 && count !== 0) {
                        // const tempoTrabalhado = this.takeHoursWork();
                        // this.setStateTimer(tempoTrabalhado.hours, tempoTrabalhado.minutes, tempoTrabalhado.seconds)
                        // this.setState({ running: true })
                        const toDate = this.toDate()
                        const totalHoras = JSON.parse(localStorage.getItem('totalHorasTrabalhadas'));
                        const horas = totalHoras.hours * 3600000;
                        const minutes = totalHoras.minutes * 60000
                        const seconds = totalHoras.seconds * 1000;

                        const absoluteHours = Math.floor(horas);

                        const absoluteMinutes = Math.floor(minutes);

                        const absoluteSeconds = Math.floor(seconds);

                        const calculo = absoluteHours + absoluteMinutes + absoluteSeconds;

                        const tempo = toDate + calculo;
                        const tempo2 = this.msToTime(tempo)
                        this.setStateTimer(tempo2.hours, tempo2.minutes, tempo2.seconds)
                    }
                    // pontos de intervalo
                    if (count % 2 === 0) {
                        const tempoUltimoPontoIntervalo = this.takeHours('horaIntervalo', 1);
                        this.setStateTimer(tempoUltimoPontoIntervalo.hours, tempoUltimoPontoIntervalo.minutes, tempoUltimoPontoIntervalo.seconds)
                        this.setState({ running: true })
                    }
                }
            }
        } catch (error) {
            this.setState({ running: false })
        }

        try {
            const firstLogin = LoginsDia();
            let tipoDeHorario;
            const contador = JSON.parse(localStorage.getItem('count'));
            if (contador % 2 === 0) {
                tipoDeHorario = false
            }
            if (contador % 2 !== 0) {
                tipoDeHorario = true
            }

            if (this.state.primeiroLoginJiraya === true && contador === 0) {
                this.setState({
                    mensagemTipoHorario: '',
                    mensagemClass: ''
                })
            } else if (this.state.primeiroLoginJiraya === false && firstLogin.length === 1 && contador === 0) {
                this.setState({
                    mensagemTipoHorario: 'Marque seu primeiro ponto do dia',
                    mensagemClass: 'horario-intervalo padding-top-class'
                })
            } else {
                const data = new Date();
                if (data.getHours() >= 11 && data.getHours() < 14) {
                    if (tipoDeHorario === false) {
                        this.setState({
                            mensagemTipoHorario: 'Você está em horário de almoço',
                            mensagemClass: 'horario-intervalo padding-top-class'
                        })
                    }

                    if (tipoDeHorario === true) {
                        this.setState({
                            mensagemTipoHorario: 'Você está em horário de trabalho',
                            mensagemClass: 'horario-work padding-top-class'
                        })
                    }
                } else if (tipoDeHorario === true) {
                    this.setState({
                        mensagemTipoHorario: 'Você está em horário de trabalho',
                        mensagemClass: 'horario-work padding-top-class'
                    })
                } else if (tipoDeHorario === false) {
                    this.setState({
                        mensagemTipoHorario: 'Você está em horário de intervalo',
                        mensagemClass: 'horario-intervalo padding-top-class'
                    })
                }

            }

        } catch (error) {

        }
    }

    componentWillMount() {
        this.setState({
            running: true
        })
        this.updateScreen()


        // const ultimaUpdate = new Date(JSON.parse(localStorage.getItem('ultimoHorario'))).getTime();        
        // const arrayTempo = this.msToTime(tempo3)
        // console.log(arrayTempo);
        // this.setStateTimer(arrayTempo.hours, arrayTempo.minutes, arrayTempo.seconds)
    }

    componentDidMount() {
        this.updateScreen()
    }

    montarAviso() {
        if (localStorage.getItem('history') === null) {
            return (
                <p className='text-centered'>Faça sua primeira marcação no <strong>Jiraya</strong></p>
            )
        } else if (this.state.userLocal !== this.state.userSession) {
            return (
                <p className='text-centered'>Faça sua primeira marcação no <strong>Jiraya</strong></p>
            )
        } else {
            const registers = JSON.parse(localStorage.getItem('history'))
            dayLastRegister = registers[registers.length - 1].day;
            monthLastRegister = registers[registers.length - 1].month;
            yearLastRegister = registers[registers.length - 1].year;
            hourLastRegister = registers[registers.length - 1].hour;
            minutesLastRegister = registers[registers.length - 1].minutes;
            secondsLastRegister = registers[registers.length - 1].seconds;

            data = new Date(yearLastRegister, monthLastRegister - 1, dayLastRegister, hourLastRegister, minutesLastRegister, secondsLastRegister)

            return (
                <p id='teste' className='text-centered'>Última marcação aproximadamente <Moment interval={1000} locale='pt-br' fromNow>{data}</Moment></p>
            )
        }
    }

    update() {
        this.props.onUpdate(true)
        this.updateScreen()
        this.setState({ ...this.state, clickMarcarPonto: true })
    }

    render() {
        return (
            <Fragment>

                <ToastContainer />

                <div className='ponto-style'>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <Timer mensagemClass={`${this.state.mensagemClass}`} tipoDeHorario={this.state.mensagemTipoHorario} primeiroLoginJiraya={this.state.primeiroLoginJiraya} running={this.state.running} hours={this.state.hours} minutes={this.state.minutes} seconds={this.state.seconds} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12 mb-3 text-center'>
                            <TimerAtual />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12 text-center'>
                            {this.montarAviso()}
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-4'>
                            <Button id='btnAdd' onClick={this.showModalAdd}>+</Button>
                        </div>
                        <div className='col-sm-8'>
                            <Button id='btn-bater-ponto' onClick={this.handleClick} disabled={this.state.isLoading === true} className={this.state.isLoading === true ? 'fourth isLoading btn-bater-ponto-loading' : 'fourth btn-bater-ponto'}>{this.state.isLoading === true ? 'Carregando' : 'Marcar Ponto'}</Button>
                        </div>
                        
                    </div>
                    
                    <div className='row'>
                        <div className='col-sm-12'>
                            <Eegg />
                        </div>
                    </div>
                </div>
                <ModalAdicionar visible={this.state.showModalAdd} close={this.hideModalAdd} />
            </Fragment>
        );
        
    }
}


export default Ponto;