import React, { Component, Fragment } from 'react';
import { Jumbotron } from 'react-bootstrap'

import './Timer.css'

class Timer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            seconds: this.props.seconds,
            minutes: this.props.minutes,
            hours: this.props.hours,
            running: true,
            primeiroLoginJiraya: this.props.primeiroLoginJiraya,
            tempoAtual: null
        }
        this.renderAviso = this.renderAviso.bind(this)
        this.renderClock = this.renderClock.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        this.update(nextProps.seconds, nextProps.minutes, nextProps.hours)
        this.setState({
            running: nextProps.running
        })
        this.renderClock()
    }

    tick() {
        let seconds = this.state.seconds + 1;
        let minutes = this.state.minutes;
        let hours = this.state.hours;

        if (seconds === 60) {
            seconds = 0;
            minutes = minutes + 1;
        }

        if (minutes === 60) {
            seconds = 0;
            minutes = 0;
            hours = hours + 1;
        }

        this.update(seconds, minutes, hours);
    }

    update(seconds, minutes, hours) {
        this.setState({
            seconds: seconds,
            minutes: minutes,
            hours: hours
        });
    }

    zero(value) {
        return value < 10 ? `0${value}` : value;
    }

    componentWillMount() {
        this.interval = setInterval(() => {
            this.setState({
                tempoAtual: new Date().toLocaleString()
            })
        }, 1000)
    }

    componentDidMount() {
        if (this.state.running) {
            this.interval = setInterval(() => {
                this.tick();
                localStorage.setItem('ultimoHorario', JSON.stringify(this.state))
            }, 1000)
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    renderClock() {
        const count = localStorage.getItem('count');
        if (count === null) {
            return (
                <Fragment>
                    <span>{this.zero(0)}:</span>
                    <span>{this.zero(0)}:</span>
                    <span>{this.zero(0)}</span>
                </Fragment>
            )
        } else if (this.state.primeiroLoginJiraya && JSON.parse(count) === 0) {
            return (
                <span>00:00:00</span>
            )
        } else {
            return (
                <Fragment>
                    <span>{this.zero(this.state.hours)}:</span>
                    <span>{this.zero(this.state.minutes)}:</span>
                    <span>{this.zero(this.state.seconds)}</span>
                </Fragment>
            )
        }
    }

    componentDidUpdate() {
        this.renderClock()
    }

    renderAviso() {

        return (
            <p className={this.props.mensagemClass}>{this.props.tipoDeHorario}</p>
        )

    }

    render() {
        return (
            <Fragment>
                <div className='row'>
                    <div className='col-sm-12 text-center'>
                        {this.renderAviso()}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 text-center'>
                        <Jumbotron className='style-jumbo'>
                            <div className='timer timer-size'>
                                {this.renderClock()}
                            </div>
                        </Jumbotron>
                    </div>
                </div>
                {/* <div className='row mb-3'>
                    <div className='col-sm-12 text-class-style'>
                        <span className='span-tempo-atual'><strong>{this.state.tempoAtual}</strong></span>
                    </div>
                </div> */}
            </Fragment>
        );
    }
}


export default Timer;