import React, { Component } from 'react';

class TimerAtual extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tempoAtual: null
        }
    }

    componentWillMount() {
        this.interval = setInterval(() => {
            this.setState({
                tempoAtual: new Date().toLocaleString()
            })
        }, 1000)
    }

    render() {
        return (
            <h5 className='style-timer-atual'><strong>{this.state.tempoAtual}</strong></h5>
        );
    }
}

export default TimerAtual;