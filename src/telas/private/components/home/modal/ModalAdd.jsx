import React, { Component } from 'react'
import Modal from 'react-bootstrap4-modal'
import './Modal.css'
import { SaveHistory } from '../../../../../services/LocalStorage'


class ModalAdicionar extends Component {

    constructor(props) {
        super(props);

        var today = new Date(ano,mes,dia),
        dataHoje = today.getDate() + '/' + today.getMonth() +1 + '/' + today.getFullYear();
        var now = new Date(),
        timeNow = now.getHours() + ':' + now.getMinutes();

        this.state = {
            visible: this.props.visible,
            time: timeNow,
            motive: '',
            dataNow: dataHoje,
            modalDateTime: 0
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)

    }

    componentDidUpdate() {
        if (this.state.visible !== this.props.visible) {
            this.setState({
                visible: this.props.visible
            })
        }

    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
        console.log(this.state.time)
    }

     handleSave() {
        const time = this.state.time;
        const motive = this.state.motive;
        const data = this.state.dataNow;
        const config = JSON.stringify([{ dia: `${data}`, hora: `${time}`, motive: `${motive}` }]);
        localStorage.setItem('config', config)
    }


    handleClick() {
        
        SaveHistory({
            // 'id': `${sessionStorage.getItem('id')}`,
            // 'username': `${sessionStorage.getItem('username')}`,
            'day': `${this.state.dataNow.getDate()}`,
            'month': `${this.state.dataNow.getMonth() + 1}`,
            'year': `${this.state.dataNow.getFullYear()}`,
            'hour': `${this.state.dataNow.getHours()}`,
            'minutes': `${this.state.dataNow.getMinutes()}`,
            'seconds': `${this.state.dataNow.getSeconds()}`
        }, sessionStorage.getItem('username'))

        // this.handleSave()
        this.setState({ visible: false })
    }

    render() {
        return (
            <Modal visible={this.state.visible} >
                <div className="modal-header">
                    <h5 className="modal-title">Adicionar marcação</h5>
                    
                </div>
                <div className="modal-body">
                    <div className='form-content'>
                        
                            <div className='col-md-4 form-group'>
                                <label htmlFor='inputData' className='label-control'>
                                    Data: 
                                </label>
                                <input name='dataNow' id='inputData' value={this.state.dataNow} onChange={this.handleChange} className='form-control' readOnly></input>
                               <br />

                                <label htmlFor='inputTime' className='label-control'>
                                    Horário:
                                </label>
                                <input name='time' type='time' value={this.state.time} onChange={this.handleChange} id='inputTime' className='form-control'></input>
                            </div>
                            <div className='col-sm-12 form-group'>
                                <label htmlFor='inputMotive' className='label-control'>
                                    Motivo:
                                </label>
                                <textarea placeholder='Motivo da adição de ponto' rows='2' name='motive' value={this.state.motive} onChange={this.handleChange} id='inputMotive' className='form-control' autoFocus></textarea>
                            </div>
                        </div>
                    </div>
                
                <div className="modal-footer">
                    <button type='button' className='btn btn-primary' onClick={this.handleClick}>Enviar</button>
                </div>
            </Modal >
        );
    }
}

export default ModalAdicionar