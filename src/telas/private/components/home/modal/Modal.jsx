import React, { Component } from 'react'
import Modal from 'react-bootstrap4-modal';

import './Modal.css'

class ModalSettings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.visible,
            nomeView: '',
            emailCoordenador: '',
            jornadaTrabalho: '',
            numeroFuncionario: 0
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleSave = this.handleSave.bind(this)
    }

    componentWillMount() {
        if (localStorage.getItem('config') !== null) {
            const config = JSON.parse(localStorage.getItem('config'));
            this.setState({ nomeView: config[0].nome })
            this.setState({ emailCoordenador: config[0].email })
            this.setState({ jornadaTrabalho: config[0].jornada })
            this.setState({ numeroFuncionario: config[0].numeroFuncionario })
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSave() {
        const nomeView = this.state.nomeView;
        const emailCoordenador = this.state.emailCoordenador;
        const jornada = this.state.jornadaTrabalho;
        const numeroFuncionario = this.state.numeroFuncionario;
        const config = JSON.stringify([{ nome: `${nomeView}`, email: `${emailCoordenador}`, jornada: `${jornada}`, numeroFuncionario: `${numeroFuncionario}` }]);
        localStorage.setItem('config', config)
    }

    handleClick() {
        this.handleSave()
        this.setState({ visible: false })
    }

    render() {
        return (
            <Modal visible={this.state.visible} >
                <div className="modal-header">
                    <h5 className="modal-title">Configurações</h5>
                </div>
                <div className="modal-body">
                    <div className='form-content'>
                        <div className='row mb-3'>
                            <div className='col-sm-12 form-group'>
                                <label htmlFor='campo-nome' className='label-control'>
                                    Nome Completo
                                </label>
                                <input name='nomeView' value={this.state.nomeView} onChange={this.handleChange} id='campo-nome' className='form-control'></input>
                            </div>
                        </div>
                        <div className='row mb-3'>
                            <div className='col-sm-12 form-group'>
                                <label htmlFor='campo-email-coordenador' className='label-control'>
                                    E-mail do seu Coordenador
                                </label>
                                <input name='emailCoordenador' value={this.state.emailCoordenador} onChange={this.handleChange} id='campo-email-coordenador' className='form-control'></input>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col-sm-6 form-group'>
                                <label htmlFor='select-jornada' className='label-control'>
                                    Jornada de trabalho
                                </label>
                            </div>
                            <div className='col-sm-5 form-group'>
                                <label htmlFor='campo-numero-funcionario' className='label-control'>
                                    Número Funcionário
                                </label>
                            </div>
                        </div>
                        <div className='row row-menos-margin'>
                            <div className='col-sm-4 form-group'>
                                <select name='jornadaTrabalho' className='form-control' onChange={this.handleChange} id='select-jornada' value={this.state.jornadaTrabalho}>
                                    <option value></option>
                                    <option value='08:48'>08:48</option>
                                    <option value='08:00'>08:00</option>
                                    <option value='07:20'>07:20</option>
                                    <option value='06:00'>06:00</option>
                                    <option value='04:00'>04:00</option>
                                </select>
                            </div>
                            <div className='col-sm-4 offset-sm-2'>
                                <input name='numeroFuncionario' type='number' value={this.state.numeroFuncionario} onChange={this.handleChange} id='campo-numero-funcionario' className='form-control'></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type='button' className='btn btn-primary' onClick={this.handleClick}>Salvar</button>
                </div>
            </Modal >
        );
    }
}

export default ModalSettings