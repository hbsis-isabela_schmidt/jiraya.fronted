import React, { Component, Fragment } from 'react'
import DayHistory from './dayHistory/History'
import BaterPonto from './baterPonto/Ponto'

import Modal from './modal/Modal'

import './custom.css'

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            primeiroLoginJiraya: false,
            clickMarcarPonto: false,
            userSession: '',
            userLocal: ''
        }
        this.updateHandler = this.updateHandler.bind(this)
    }

    updateHandler = val => {
        this.setState({
            clickMarcarPonto: val
        })
    }

    showModal = () => {
        this.setState({ showModal: true })
    }

    hideModal = () => {
        this.setState({ showModal: false })
    }

    componentDidMount() {
        if (this.state.userLocal !== this.state.userSession) {
            localStorage.removeItem('history')
            localStorage.removeItem('horaTrabalho')
            localStorage.removeItem('count')
            localStorage.removeItem('username')
            localStorage.removeItem('horaIntervalo')
            localStorage.removeItem('config')
        }
        this.forceUpdate()
    }

    componentWillMount() {
        if (localStorage.getItem('username') !== null) {
            this.setState({ userLocal: localStorage.getItem('username') })
            this.setState({ userSession: sessionStorage.getItem('username') })
        } else {
            this.setState({ userLocal: '' })
        }        

        //primeiro login no jiraya
        const local = JSON.parse(localStorage.getItem('login'));
        if (localStorage.getItem('login') !== null) {
            if (local.length === 1 && localStorage.getItem('config') === null) {
                this.setState({ showModal: true })
                this.setState({ primeiroLoginJiraya: true })
            }

            const config = JSON.parse(localStorage.getItem('config'));
            if (localStorage.getItem('config') !== null) {
                if (config[0].nome.length === 0 || config[0].email.length === 0 || config[0].jornada.length === 0) {
                    this.setState({ showModal: true })
                }
            }
        }
    }

    render() {
        return (
            <Fragment>
                <div className='row'>
                    <div className='col-md-5 offset-1'>
                        <BaterPonto onUpdate={this.updateHandler} primeiroLoginJiraya={this.state.primeiroLoginJiraya} /> 
                    </div>

                    <div className='col-md-5 bg-red'>
                        <DayHistory />
                    </div>
                </div>

                <Modal visible={this.state.showModal} close={this.hideModal} />

            </Fragment>
        )
    }

}