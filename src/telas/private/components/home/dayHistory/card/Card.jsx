import React, { Component } from 'react';
import './Card.css'
import Swal from 'sweetalert2'

import { PostData } from '../../../../../../services/PostData'

let success;

class Card extends Component {

    constructor(props) {
        super(props);

        this.state = {
            day: props.day <= 9 ? `0${props.day}` : props.day,
            month: props.month <= 9 ? `0${props.month}` : props.month,
            year: props.year,
            hour: props.hour <= 9 ? `0${props.hour}` : props.hour,
            minute: props.min <= 9 ? `0${props.min}` : props.min,
            id: props.id,
            second: props.sec <= 9 ? `0${props.sec}` : props.sec,
            reason: '',
            user: '',
            emailTo: '',
            nameView: ''
        }

        this.handleDoubleClick = this.handleDoubleClick.bind(this)
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleDoubleClick() {
        Swal({
            title: 'Motivo da alteração',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Enviar!',
            confirmButtonColor: '#2870B2',
            cancelButtonColor: '#c5d0e1',
            showLoaderOnConfirm: true,
            preConfirm: async (data) => {
                const config = JSON.parse(localStorage.getItem('config'));
                const nomeView = config[0].nome;
                const emailTo = config[0].email;
                this.setState({ reason: data })
                this.setState({ user: sessionStorage.username })
                this.setState({ nameView: nomeView })
                this.setState({ emailTo: emailTo })
                try {
                    const response = await PostData('email', this.state);
                    if (response.success) {
                        success = true;
                    }
                    else {
                        success = false;
                    }
                }
                catch (resp) {
                    if (success === false) {
                        this.setState({ reason: '' });
                        Swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Alguma coisa deu errado! A janela fechará automaticamente',
                            timer: 3000
                        });
                    }
                }
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((bool) => {
            if (!bool.dismiss && success === false) {
                this.setState({ reason: '' })
                Swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Alguma coisa deu errado! A janela fechará automaticamente.',
                    showConfirmButton: false,
                    showCloseButton: false,
                    timer: 5000
                })
            }
            if (!bool.dismiss && success === true) {
                this.setState({ reason: '' })
                this.setState({ user: '' })
                Swal({
                    type: 'success',
                    title: 'Email enviado!',
                    text: 'A janela fechará automaticamente.',
                    showConfirmButton: false,
                    showCloseButton: false,
                    timer: 3000
                })
            }
        })
    }

    render() {
        return (
            <div className='col-sm-6'>
                <div className="card card-style" onDoubleClick={this.handleDoubleClick}>
                    <div className="card-body">
                        <div className='row'>

                            <div className='col-sm-6'>
                                <h5 className="card-title">{`${this.state.day}/${this.state.month}/${this.state.year}`}</h5>
                            </div>

                            <div className='col-sm-6'>
                                <h5 className="card-title">{`${this.state.hour}:${this.state.minute}:${this.state.second}`}</h5>
                            </div>

                            <input type='hidden' value={`${this.state.id}`} />

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;