import React, { Component } from 'react';
import Card from './card/Card'

let storageHist
let newStorageHist

class History extends Component {

    constructor(props) {
        super(props);

        this.getDayHistoryForCards = this.getDayHistoryForCards.bind(this)
        this.renderCards = this.renderCards.bind(this)
    }

    getDayHistoryForCards() {
        try {
            storageHist = JSON.parse(localStorage.getItem('history'))
            newStorageHist = []
            const d = new Date();

            for (let i = 0; i < storageHist.length; i++) {

                if (storageHist[i].day === d.getDate().toString() && storageHist[i].month === (d.getMonth() + 1).toString() && storageHist[i].year === d.getFullYear().toString()) {

                    newStorageHist.push(storageHist[i])

                }

            }

        } catch (error) {
            return (
                <div id='divTesteTexto'>
                    <p id='testeTexto'>Nenhuma marcação de ponto realizada hoje!</p>
                </div>
            )
        }

    }

    renderCards() {
        let cards = newStorageHist.map(props => {
            return <Card key={`${props.id}`} id={`${props.id}`} day={`${props.day}`} month={`${props.month}`} year={`${props.year}`} hour={`${props.hour}`} min={`${props.minutes}`} sec={`${props.seconds}`} />
        })
        
        return (
            cards
        )
    }

    componentDidUpdate() {
        this.renderCards()
    }

    render() {

        return (
            <div>
                <div className='row'>

                    {this.getDayHistoryForCards()}
                    {this.renderCards()}

                </div>
            </div>
        );
    }
}

export default History;