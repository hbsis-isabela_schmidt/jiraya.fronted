import React, { Component, Fragment } from 'react'
import '../../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import './History.css'
import Table from './table/Table'
import { Jumbotron } from 'react-bootstrap'
import { GetHours } from '../../../../services/Historico';

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            usuarioRedeHBSIS: '',
            dataInicio: '',
            dataFim: '',
            numeroFuncionario: '',
            data: [],
            message: 'Nenhum registro encontrado'
        }
        this.sendDate = this.sendDate.bind(this)
    }

    componentWillMount() {
        const config = JSON.parse(localStorage.getItem('config'));
        this.setState({
            numeroFuncionario: config[config.length - 1].numeroFuncionario,
            usuarioRedeHBSIS: sessionStorage.getItem('username')
        })
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    sendDate() {
        if (this.state.dataInicio && this.state.dataFim) {
            this.setState({ message: 'Carregando...', data: [] })
            GetHours('jira/registrosPontoDetalhe', this.state)
                .then((result) => {
                    this.setState({
                        data: result
                    })
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    render() {
        return (
            <Fragment>
                <div className='row'>
                    <div className='col-sm-8 offset-sm-2'>
                        <Jumbotron className='shadow-jumb'>
                            <div className='row'>
                                <div className='col-sm-3 offset-sm-1'>
                                    <div className='form-group'>
                                        <label className='control-label' htmlFor='campo-data'>Data Inicial</label>
                                        <input onChange={this.handleChange} value={this.state.dataInicio} name='dataInicio' id='campo-data' type='date' className='form-control form-control-width'></input>
                                    </div>
                                </div>
                                <div className='col-sm-3'>
                                    <div className='from-group '>
                                        <label className='control-label' htmlFor='campo-data2'>Data Final</label>
                                        <input onChange={this.handleChange} value={this.state.dataFim} name='dataFim' id='campo-data2' type='date' className='form-control form-control-width'></input>
                                    </div>
                                </div>
                                <div className='col-sm-3'>
                                    <input className='fourth ' id='button-history' onClick={this.sendDate} type="submit"></input>
                                </div>
                            </div>
                        </Jumbotron>
                    </div>
                </div>

                <div className='row'>
                    <div className='col-sm-12'>
                        <div style={{ marginBottom: 50 }}>
                            <Table data={this.state.data} message={this.state.message} />
                        </div>
                    </div>
                </div>
            </Fragment >
        )

    }

}