import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'


class History extends Component {

    render() {
        const options = {
            page: 1,
            noDataText: this.props.message,
            sizePerPageList: [{
                text: '10', value: 10
            }, {
                text: '20', value: 20
            }, {
                text: '50', value: 50
            }, {
                text: 'All', value: this.props.data.length
            }],
            paginationPosition: 'bottom'
        }

        return (
            <BootstrapTable
                tableStyle={{ backgroundColor: '#fff', boxShadow: '0 5px 15px 0 #95a3b7' }}
                data={this.props.data}
                pagination={true}
                options={options}
            >
                <TableHeaderColumn dataSort={true} dataFormat={format} dataField='dataBatida'isKey={true}>Data</TableHeaderColumn>
                <TableHeaderColumn  dataField='horarioBatida'>Horário</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}
function format(cell){
    return new Date(cell).toLocaleDateString();
}

export default History;
