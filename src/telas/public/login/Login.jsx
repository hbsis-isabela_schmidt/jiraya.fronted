import React, { Component, Fragment } from "react";
import "./Login.css";
import { PostData } from '../../../services/PostData'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Redirect } from 'react-router-dom'
import { SaveLogin } from '../../../services/LocalStorage'
import LoadingGif from '../../../assets/gifs/isLoading.gif'
import { LoginsDia } from '../../../services/Logins'

import Logo from '../../../assets/images/logo-150x15.png'

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            redirect: false,
            isLoading: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.handleKeyPress = this.handleKeyPress.bind(this)
    }

    notifyLoginError = () => {
        toast.error("Erro na autenticação!", {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 3000
        });
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = () => {
        if (this.state.username && this.state.password) {
            this.setState({ isLoading: true })
            PostData('ronda', this.state)
                .then((result) => {
                    let responseJson = result
                    sessionStorage.setItem('username', responseJson.username)
                    sessionStorage.setItem('password', responseJson.password)
                    sessionStorage.setItem('token', responseJson.token)
                    const d = new Date();
                    SaveLogin({ 'username': `${this.state.username}`, 'day': `${d.getDate()}`, 'month': `${d.getMonth() + 1}`, 'year': `${d.getFullYear()}` })
                    this.setState({ isLoading: false })
                    this.setState({ redirect: true })
                })
                .catch((res) => {
                    this.setState({ isLoading: false })
                    this.notifyLoginError()
                })
        }
    }

    componentWillUnmount() {
        if (localStorage.getItem('login') !== null) {
            const logins = LoginsDia()
            if (logins.length === 1) {
                localStorage.setItem('count', 0)
                localStorage.setItem('horaTrabalho', null)
                localStorage.setItem('horaIntervalo', null)
                localStorage.removeItem('totalHorasTrabalhadas')
            }
        }
    }

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.handleSubmit();
        }
    }

    render() {

        if (this.state.redirect) {
            return (<Redirect to={'/home'} />)
        }

        if (sessionStorage.getItem('token')) {
            return (<Redirect to={'/home'} />)
        }

        return (

            <Fragment>

                <ToastContainer />

                <div className='wrapper fadeInDown'>

                    <div id='formContent'>

                        <div className='fadeIn first'>
                            <img src={Logo} id="icon" alt="User Icon" />
                            <h2>Jiraya</h2>
                        </div>

                        <input
                            type="text"
                            id="txtUser"
                            className="fadeIn second"
                            name="username"
                            placeholder="Usuário"
                            value={this.state.user}
                            onChange={this.handleChange}
                            onKeyPress={this.handleKeyPress}
                        >
                        </input>
                        <input
                            type="password"
                            id="txtPassword"
                            className="fadeIn third"
                            name="password"
                            placeholder="Senha"
                            value={this.state.password}
                            onChange={this.handleChange}
                            onKeyPress={this.handleKeyPress}
                        >
                        </input>
                        <input type="submit" id='btnLogar' className="fadeIn fourth" value={this.state.isLoading !== true ? "Entrar" : 'Carregando'} onClick={this.handleSubmit} disabled={!this.validateForm() || this.state.isLoading === true}></input>
                        {this.state.isLoading === true ? <img alt='isLoading' className='gif' src={LoadingGif}></img> : null}
                    </div>

                </div>

            </Fragment>
        );
    }
}