export function HistoryDia() {
    const d = new Date();
    const history = JSON.parse(localStorage.getItem('history'));
    const newArray = [];
    for (let i = 0; i < history.length; i++) {
        if (history[i].day === d.getDate().toString() && history[i].month === (d.getMonth() + 1).toString() && history[i].year === d.getFullYear().toString() && history[i].username === sessionStorage.getItem('username')) {
            newArray.push(history[i])
        }
    }
    return newArray
}

export function PontosTrabalhoDia() {
    const d = new Date();
    const horaTrabalho = JSON.parse(localStorage.getItem('horaTrabalho'));
    const newArray = [];
    for (let i = 0; i < horaTrabalho.length; i++) {
        if (horaTrabalho[i].day === d.getDate().toString() && horaTrabalho[i].month === (d.getMonth() + 1).toString() && horaTrabalho[i].year === d.getFullYear().toString() && horaTrabalho[i].username === sessionStorage.getItem('username')) {
            newArray.push(horaTrabalho[i])
        }
    }
    return newArray
}

export function PontosIntervaloDia() {
    const d = new Date();
    const horaIntervalo = JSON.parse(localStorage.getItem('horaIntervalo'));
    const newArray = [];
    for (let i = 0; i < horaIntervalo.length; i++) {
        if (horaIntervalo[i].day === d.getDate().toString() && horaIntervalo[i].month === (d.getMonth() + 1).toString() && horaIntervalo[i].year === d.getFullYear().toString() && horaIntervalo[i].username === sessionStorage.getItem('username')) {
            newArray.push(horaIntervalo[i])
        }
    }
    return newArray
}