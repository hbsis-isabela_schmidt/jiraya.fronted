export function SaveHistory(value, username) {
    let dataStorage;
    if (!localStorage.getItem('history')) {
        dataStorage = []
    } else {
        dataStorage = JSON.parse(localStorage.getItem('history'))
    }
    let newDataStorage = dataStorage
    newDataStorage.push(value)
    localStorage.setItem('history', JSON.stringify(newDataStorage))
    localStorage.setItem('username', username)
}

export function SaveLastPoints(value, type) {
    let dataStorage = JSON.parse(localStorage.getItem(`${type}`));
    if (dataStorage === null) {
        dataStorage = []
    } else {
        dataStorage = JSON.parse(localStorage.getItem(`${type}`))
    }
    const newDataStorage = dataStorage
    newDataStorage.push(value)
    localStorage.setItem(`${type}`, JSON.stringify(newDataStorage))
}

export function SaveLogin(value) {
    let localStore
    if (!localStorage.getItem('login')) {
        localStore = []
    } else {
        localStore = JSON.parse(localStorage.getItem('login'))
    }
    let newDataStorage = localStore
    newDataStorage.push(value)
    localStorage.setItem('login', JSON.stringify(newDataStorage))
}