export function LoginsDia() {
    const d = new Date();
    const login = JSON.parse(localStorage.getItem('login'));
    const newArray = [];
    for (let i = 0; i < login.length; i++) {
        if (login[i].day === d.getDate().toString() && login[i].month === (d.getMonth() + 1).toString() && login[i].year === d.getFullYear().toString() && login[i].username === sessionStorage.getItem('username')) {
            newArray.push(login[i])
        }
    }
    return newArray
}