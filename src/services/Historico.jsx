const BaseUrl = `http://${window.location.hostname}:5000/`

export function GetHours(type, data) {
    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((result) => {
                resolve(result)
            })
            .catch((erro) => {
                reject(erro)
            })
    })
}
