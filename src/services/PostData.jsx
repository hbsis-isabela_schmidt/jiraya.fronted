const BaseUrl = `http://${window.location.hostname}:5000/`

function PostData(type, userData) {

    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });

    })
}

function PontoMethod(type, userData) {

    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                let responseCopy = response.clone()
                return responseCopy.json()
            })
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });
    })

}

export { PostData, PontoMethod }
